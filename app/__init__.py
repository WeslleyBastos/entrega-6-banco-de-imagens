from flask import Flask
from werkzeug.exceptions import BadRequestKeyError, NotFound
from environs import Env
from kenzie.image import MAX_CONTENT_LENGTH, FORMAT_DATA, upload_form, list_all_files, files_by_extension, download_file, donwload_file_zip
from os import environ
import os

app = Flask(__name__)

env = Env()
env.read_env()

FILES_DIRECTORY = environ.get('FILES_DIRECTORY')
MAX_CONTENT_LENGTH = int(environ.get('MAX_CONTENT_LENGTH'))
FORMAT_DATA = list(environ.get('FORMAT_DATA'))

app.config['MAX_CONTENT_LENGTH'] = MAX_CONTENT_LENGTH

if not os.path.exists(FILES_DIRECTORY):
    os.makedirs(FILES_DIRECTORY)

@app.errorhandler(413)
def error_function(_):
    return {'message': 'Arquivo maior que o esperado.'}, 413

""" if not os.path.exists(FILES_DIRECTORY):
    os.makedirs(FILES_DIRECTORY) """

@app.post('/upload')
def upload():
    return upload_form()

@app.get('/files')
def get_files():
    return list_all_files()

@app.get('/files/<string:extension>')
def get_extension_files(extension):
    return files_by_extension(extension)

@app.get('/download/<file_name>')
def get_download_file(file_name: str):
    return download_file(file_name)

@app.get('/download-zip')
def get_donwload_file_zip():
    return donwload_file_zip()