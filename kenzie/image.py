from flask import request, safe_join, jsonify, send_from_directory
import os
from environs import Env
from os import environ
from werkzeug.utils import secure_filename

env = Env()
env.read_env()

FILES_DIRECTORY = environ.get('FILES_DIRECTORY')
MAX_CONTENT_LENGTH = environ.get('MAX_CONTENT_LENGTH')
FORMAT_DATA = environ.get('FORMAT_DATA')

def upload_form():
    files_list = list(request.files)

    for file in files_list:
        received_file = request.files[file]

        filename = secure_filename(received_file.filename)
        file_extension = filename.split('.')[-1]

        if file_extension not in FORMAT_DATA:
            return {'message': 'Formato de arquivo não suportado.'}, 415
        
        if not os.path.exists(f'{FILES_DIRECTORY}/{file_extension}'):
            os.makedirs(f'{FILES_DIRECTORY}/{file_extension}')
        
        file_names = os.listdir(f'{FILES_DIRECTORY}/{file_extension}')

        if filename in file_names:
            return {'message': 'Arquvio já existente.'}, 409

        file_path = safe_join(f'{FILES_DIRECTORY}/{file_extension}', filename)

        received_file.save(file_path)

    return {'message': f'O arquivo {filename} foi upado com sucesso!'}, 201

def list_all_files():
    files_list = os.walk(FILES_DIRECTORY)

    all_files = []

    for file in list(files_list)[1:]:
        all_files.extend(file[-1])

    return jsonify(all_files), 200

def files_by_extension(extension): 
    try:
        files_list = os.listdir(f'{FILES_DIRECTORY}/{extension}')
        return jsonify(files_list)
    except FileNotFoundError:
        return {'message': 'Diretório não encontado!'}, 404

def download_file(file_name):
    extension_file = file_name.split('.')[-1]

    path = os.walk(FILES_DIRECTORY)

    if extension_file not in list(path)[0][1]:
        return {'message': 'Diretório não encontrado'}, 404
    
    return send_from_directory(directory=f'.{FILES_DIRECTORY}/{extension_file}', path=file_name, as_attachment=True), 200

def donwload_file_zip():
    file_extension = request.args.get("file_extension")
    compression_rate = request.args.get("compression_rate")
    path_zip = safe_join(f'{FILES_DIRECTORY}', file_extension)

    if not os.path.exists(path_zip):
        return {'message':'O diretório não existe.'}, 404
    
    if len(os.listdir(path_zip)) == 0:
        return {'message': 'O diretório está vazio.'}, 404

    os.system(f' zip -r - {compression_rate} {file_extension}.zip {path_zip}')

    os.system(f'mv {file_extension}.zip /tmp')

    return send_from_directory(directory='/tmp', path=f'{file_extension}.zip', as_attachment=True), 200